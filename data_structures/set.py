class Set:
    def __init__(self, args=[]):
        self._dict = {}
        for arg in args:
            self.add(arg)

    def extend(self, args):
        for arg in args:
            self.add(arg)

    def add(self, item):
        self._dict[item] = item

    def remove(self, item):
        del self._dict[item]

    def contains(self, item):
        return item in self._dict.keys()

    __contains__ = contains

    def __getitem__(self, index):
        return self._dict.keys()[index]

    def __iter__(self):
        return iter(self._dict.copy())

    def __len__(self):
        return len(self._dict)

    def items(self):
        result = self._dict.keys()
        try: sorted(result)
        except: pass
        return result

    def __copy__(self):
        return Set(self)

def union(s1, s2):
    import copy
    result = copy.copy(s1)
    for item in s2:
        result.add(item)
    return result

def intersection(s1, s2):
    result = Set()
    for item in s1:
        if s2.contains(item):
            result.add(item)
    return result

def complement(s1, s2):
    result = Set()
    for item in s2:
        if not s1.contains(item):
            result.add(item)
    return result
from data_structures.set import Set
class Graph(object):

    def __init__(self):

        self.__succ = {}
        self.__pred = {}

    def add_vertex(self, vertex):

        self.__succ[vertex] = []
        if vertex not in self.__pred.keys():
            self.__pred[vertex] = []


    def add_edge(self, vertex1, vertex2):

        self.__succ[vertex1].append(vertex2)
        if vertex2 not in self.__pred.keys():
            self.__pred[vertex2]=[]
        self.__pred[vertex2].append(vertex1)

    def add_html_file(self,vertex,links):

        self.add_vertex(vertex)
        for link in links:
            self.add_edge(vertex,link)

    def get_pred_files(self,vertex):
        return Set(self.__pred[vertex])

    def vertices(self):
        return list(self.__succ.keys())


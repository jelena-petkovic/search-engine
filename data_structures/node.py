class Node:
    def __init__(self, letter = None, data = None, count = 0):
        self.letter = letter
        self.data = data
        self.count = count
        self.children = dict()

    def addChild(self, key, data = None):
        self.children[key] = Node(key, data)
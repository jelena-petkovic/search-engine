from data_structures.node import Node

class Trie:
    def __init__(self):
        self.head = Node()


    def add(self, word):
        word = word.lower()

        current_node = self.head
        word_finish = True

        for i in range(len(word)):
            if word[i] in current_node.children:
                current_node = current_node.children[word[i]]
            else:
                word_finish = False
                break

        if not word_finish:
            while i < len(word):
                current_node.addChild(word[i])
                current_node = current_node.children[word[i]]
                i += 1

        current_node.data = word
        current_node.count += 1

    def isWord(self, wordForSearch):
        word = wordForSearch.lower()

        if word == '':
            return False
        if word is None:
            print("Error")

        current_node = self.head
        exists = True

        for letter in word:
            if letter in current_node.children:
                current_node = current_node.children[letter]
            else:
                exists = False
                break
        if exists:
            if current_node.data is None:
                exists = False

        return exists, current_node.count
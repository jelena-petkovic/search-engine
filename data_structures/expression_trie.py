from utility.search_process import SearchProcess
from data_structures import set as s


#expression tree node
class Et:

    # Constructor to create a node
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

# A utility function to check if 'word'
# is an operator
def isOperator(word):
    if (word == '&&' or word == '||' or word == '!'):
        return True
    else:
        return False


# A utility function to calculate trie
def calculate(t):
    if t is not None:

        if not isOperator(t.value):
            words = []
            words.append(t.value)
            return SearchProcess.search(words)
        if (t.value == "&&"):
            return s.intersection(calculate(t.left), calculate(t.right))
        if (t.value == "||"):
            return s.union(calculate(t.left), calculate(t.right))
        if (t.value == "!"):
            return s.complement(calculate(t.left), s.Set(list(SearchProcess.dictionary_of_tries.keys())))



# Returns root of constructed tree for
# given postfix expression
def constructTree(postfix):
    stack = []

    # Traverse through every word of input expression
    for word in postfix:

        # if operand, simply push into stack
        if not isOperator(word):
            t = Et(word)
            stack.append(t)

            # Operator
        else:

            # Pop two top nodes
            t = Et(word)
            if word == "!":
                t1 = Et("word")
            else:
                t1 = stack.pop()

            t2 = stack.pop()

            # make them children
            t.right = t1
            t.left = t2

            # Add this subexpression to stack
            stack.append(t)

            # Only element  will be the root of expression tree
    t = stack.pop()

    return t

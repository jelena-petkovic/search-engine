from data_structures.trie import Trie
from utility import pars
from data_structures.graph import Graph
import os
from data_structures import set as s


class SearchProcess:

    html_graph = Graph()
    dictionary_of_tries = {}

    def load_data(path):
        parser = pars.Parser()
        for root, dirs, files in os.walk(path):
            for file in files:
                if file.endswith(".html"):
                    links, words = parser.parse(os.path.join(root, file))
                    SearchProcess.html_graph.add_html_file(os.path.join(root, file), links)
                    t = Trie()
                    for word in words:
                        t.add(word)
                    SearchProcess.dictionary_of_tries[os.path.join(root, file)] = t

    def search(words):
        html_set = s.Set()
        for path in SearchProcess.dictionary_of_tries.keys():
            for word in words:
                if SearchProcess.dictionary_of_tries[path].isWord(word) ==  False:
                    continue
                word_exists = SearchProcess.dictionary_of_tries[path].isWord(word)[0]
                if word_exists:
                    html_set.add(path)
                    break

        return html_set

    def search_with_logic(logical_operator, word1, word2):
        words1 = []
        words2 = []
        words1.append(word1)
        words2.append(word2)
        html_set1 = SearchProcess.search(words1)
        html_set2 = SearchProcess.search(words2)
        if logical_operator == "AND":
            result_set = s.intersection(html_set1, html_set2)
        elif logical_operator == "OR":
            result_set = s.union(html_set1, html_set2)
        else:
            data_set = s.Set(list(SearchProcess.dictionary_of_tries.keys()))
            result_set = s.intersection(html_set1, s.complement(html_set2, data_set))
        return result_set

    def sort_files(html_set, words):

        words_count = {}
        link_weight = {}
        for html in html_set:
            w_count = 0;
            for word in words:
                [word_exists, word_count] = SearchProcess.dictionary_of_tries[html].isWord(word)
                w_count += word_count
            words_count[html] = w_count
            links = SearchProcess.html_graph.get_pred_files(html)
            link_weight[html] = 0.8 * len(s.intersection(links, html_set)) + 0.4 * len(s.complement(html_set, links))
        dictionary_of_weight = {}
        for html in html_set:
            dictionary_of_weight[html] = words_count[html] + link_weight[html]
        sorted_files = []
        for html in html_set:
            max_weight_html = max(dictionary_of_weight, key=dictionary_of_weight.get)
            sorted_files.append([max_weight_html, dictionary_of_weight[max_weight_html]])
            dictionary_of_weight.pop(max_weight_html)

        return sorted_files

    def print_files(files):
        if len(files) < 5:
            n_pages = len(files)
        else:
            n_pages = 5;
        curr_page = 0;
        while True:
            print(len(files), ": number of found files")
            if curr_page + n_pages > len(files):
                for i in range(len(files) - curr_page):
                    print(files[curr_page + i][0], files[curr_page + i][1])
            else:
                for i in range(n_pages):
                    print(files[curr_page + i][0], files[curr_page + i][1])
            type = input(
                "\n type 1 for previous page \n type 2 for next page \n type 3 to change number of files per page \n type 4 for next search \n type 5 to exit \n\n")
            if not type.isdigit():
                continue
            type = int(type)
            if type == 1:
                curr_page -= n_pages
                if curr_page < 0:
                    curr_page = 0
            elif type == 2:
                if curr_page + n_pages < len(files):
                    curr_page += n_pages
            elif type == 3:
                temp = input("Enter your number :")
                if not temp.isdigit():
                    continue
                temp = int(temp)
                if temp > 0 and temp <= len(files):
                    n_pages = temp
                else:
                    print("Error: Input number not in range!")
            elif type == 4:
                break;
            elif type == 5:
                quit()
            else:
                print("Error: Input number not in range!")
import os
from data_structures import expression_trie, conversion_infix_to_postfix
from utility.search_process import SearchProcess

def is_format_with_logic(words):
    if ( words[1].upper() == "AND" or words[1].upper() == "OR" or words[1].upper() == "NOT" ) and not ( words[0].upper() == "AND" or words[0].upper() == "OR" or words[0].upper() == "NOT" ) and not ( words[2].upper() == "AND" or words[2].upper() == "OR" or words[2].upper() == "NOT" ):
        return True;
    else:
        return False

def is_format_without_logic(words):
    for word in words:
        if word.upper() == "AND" or word.upper() == "OR" or word.upper() == "NOT":
            print("Error: You cannot search AND,OR and NOT!")
            return False
    else:
        return True

def is_complex_format(words):
    if words[0] == "||" or words[0] == "&&" or words[0]==")" or "" in words:
        print("wrong format")
        return False
    elif words[len(words)-1] == "||" or words[len(words)-1] == "!" or words[len(words)-1] == "&&" or words[len(words)-1] == "(":
        print("wrong format")
        return False
    elif words.count("(") != words.count(")"):
        print("wrong format")
        return False
    else:
        prolaz = True
        operators= ["&&" , "||" , "!"]
        operators2 = ["&&" ,"||"]
        for i in range(0,len(words)-1):
            if(words[0]=="!" and (words[1] == "||" or words[1] =="&&")):
                print("wrong format")
                prolaz=False
                break
            elif(words[i] in operators2 and words[i+1] in operators2):
                print("wrong format")
                prolaz = False
                break
            elif(words[i] == "!" and words[i+1] in operators):
                print("wrong format")
                prolaz = False
                break
            elif(words[i] == "(" and (words[i+1] == "&&" or words[i+1] == "||" or words[i+1] == ")")):
                print("wrong format")
                prolaz = False
                break
            elif (words[i+1] == ")" and ( words[i] in operators)):
                print("wrong format")
                prolaz = False
                break

        return prolaz


if __name__ == '__main__':

    path = "PathToDirectory"
    while not os.path.isdir(path):
        path = input("enter directory path:")
    print("loading data...")
    SearchProcess.load_data(path)

    while True:
        type = input("\n type 1 for elementary search \n type 2 for complex search \n type 3 to exit \n\n")
        if not type.isdigit():
            print("Error : wrong format")
            continue
        type = int(type)
        if type == 3 :
            quit()
        str = input("Enter words you want to find: \n")
        words = str.split(" ")
        if type == 1:

            if len(words) == 3:
                if is_format_with_logic(words):
                    searched_set = SearchProcess.search_with_logic(words[1].upper(), words[0], words[2])
                    sorted_set = SearchProcess.sort_files(searched_set, [words[0], words[2]])
                    SearchProcess.print_files(sorted_set)
                else:
                    print("wrong format!")
                    continue;
            elif is_format_without_logic(words):
                searched_set = SearchProcess.search(words)
                sorted_set = SearchProcess.sort_files(searched_set,words)
                SearchProcess.print_files(sorted_set)

        elif type == 2:
            if not is_complex_format(words):
                continue

            converter = conversion_infix_to_postfix.Conversion(len(words))
            postfix = converter.infixToPostfix(words)
            postfix = postfix.split(" ")
            result_trie = expression_trie.constructTree(postfix)
            searched_set = expression_trie.calculate(result_trie)

            temp = []
            for index in range(len(words)):
                if words[index] != "&&" and words[index] != "||" and words[index] != "!" and words[index] != "(" and words[index] != ")":
                    temp.append(words[index])
            sorted_set = SearchProcess.sort_files(searched_set,temp)
            SearchProcess.print_files(sorted_set)
        



